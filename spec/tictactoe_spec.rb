require 'spec_helper'

class TicTacToe
  attr_reader :current_player

  def initialize
    @current_player = 'O'
    @plays = {'O' => [], 'X' => []}
  end

  def play(x,y)
    return false if x > 1  ||y > 1 || x < -1 || y < -1
    @plays[@current_player] << [x, y]
    @current_player = ((@current_player == 'X') ? 'O' : 'X')
  end

  def at(x,y)
    @plays.each do |player, plays|
      return player if plays.include?([x, y])
    end
    nil
  end

  def winner(x,y)
    plays = []
    plays << [x,y]
    winner_moves= [{0, -1}, {0, 0}, {0,1}]
    horizontal_winner1 = [{1, -1}, {1, 0}, {1, 1}]
    horizontal_winner2 = [{-1, -1}, {-1, 0}, {-1, 1} ]
    vertical_winner = [{-1, -1}, {-1, 0}, {-1, 1}]
    vertical_winner2 = [{1, 0}, {0, 0}, {-1, 0}]
    vertical_winner3 = [{0, 1}, { 0, 0}, {0, -1}]

    winner.each do |x, y|
    x = [0,1]
    if x[0]*3 == 0
  end

  def play(x, y)
    
  end
end

describe "TicTacToe" do
  let(:game) { TicTacToe.new }
  context "lets two players alternate turns" do
    it "assiging O for player1" do
      game.play(0, 0) # center
      expect(game.at(0, 0)).to eql 'O'
    end
    it "assigning X for player2" do
      game.play(0, 0) # center
      game.play(0, 1)
      expect(game.at(0,1)).to eql 'X'
    end
  end
  it "does not allow a player to place move off the board" do
    expect(game.play(2, 2)).to be_false
    expect(game.current_player).to eql 'O'
  end

  it "does now allow a player to place move off the board" do
    expect(game.play(-2, -2)).to be_false
    expect(game.current_player).to eql 'O'
  end

  context "has a winner when it" do
    it "has 3 of the same token on a row" do
      game.play(-1,0)
      game.play(0,0)
      game.play(0,1)
      expect()
    end
    it "has 3 of the same token on a column"
    it "has 3 of the same token diagonnally left to right"
    it "has 3 of the same token diagonnally right to left"
  end

  context "has a tie when it" do
    it "has all rows and columns filled without a winner present"
  end
end
