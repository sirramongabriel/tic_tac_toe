require './winning_rules'

class TicTacToe
  attr_reader :current_player

    def initialize(options = {})
    @current_player = 'O'
    @plays = { 'X' => [], 'O' => [] }
    @winning_rules = options[:rules] || WinnerRules.new
  end
  
   def play(x, y)
    if winner?
      puts "Player #{@current_player} wins!"
    else
      puts 'no winner yet, keep trying...'
      array = [x, y]
      @plays[@current_player] << array
    end
    switch_player unless winner?
  end

  def winner?
    @winning_rules.winner?(@plays)
  end

  private
  def switch_player
    @current_player = ((@current_player == 'X') ? 'O' : 'X')
  end
end
